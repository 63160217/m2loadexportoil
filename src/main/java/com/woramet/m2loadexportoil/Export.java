/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.m2loadexportoil;

/**
 *
 * @author User
 */
public class Export {
    private int Oil;
    private int Money;
    private int N = 1;
    private String Transway = "Land";

    public Export(int Oil, int Money) {
        this.Oil = Oil;
        this.Money = Money;
    }
    public boolean selltransport (String way){
        switch (way){
            case "Air" :
                this.N = N*1000;
                if (N > Oil){
                    printsellOil();
                }
                Oil = Oil - N;
                this.Money = Money + (N*5);
                this.N = 1;
                break;
            case "Water" :
                this.N = N*100;
                if (N > Oil){
                    printsellOil();
                }
                this.Oil = Oil - N;
                this.Money = Money + (N*10);
                this.N = 1;
                break;
            case "Land" :
                this.N = N*10;
                if (N > Oil){
                    printsellOil();
                }
                this.Oil = Oil - N;
                this.Money = Money + (N*20);
                this.N = 1;
                break;
        }
        Transway = way;
        return true;
    }
    
    public boolean selltransport (String way ,int N){
         for (int i = 0; i < N; i++){
             if (!this.selltransport (way)){
                 return false;
         }
      }
         return true;
    }
    
    public boolean selltransport () {
        return this.selltransport(Transway);
    }
    
    public boolean selltransport (int N) {
        return this.selltransport(Transway,N);
    }
    
    public boolean buytransport (String way){
        switch (way){
            case "Air" :
                this.N = N*1000;
                if (N*5 > Money){
                    printbuyOil();
                }
                this.Money = Money-(N*5);
                this.Oil = Oil+N;
                this.N = 1;
                break;
            case "Water" :
                this.N = N*100;
                if (N*10 > Money){
                    printbuyOil();
                }
                this.Money = Money-(N*10);
                this.Oil = Oil+N;
                this.N = 1;
                break;
            case "Land" :
                this.N = N*10;
                if (N*20 > Money){
                    printbuyOil();
                }
                this.Money = Money-(N*20);
                this.Oil = Oil+N;
                this.N = 1;
                break;
        }
        Transway = way;
        return true;
    }
    
    public boolean buytransport (String way ,int N){
         for (int i = 0; i < N; i++){
             if (!this.buytransport (way)){
                 return false;
         }
      }
         return true;
    }
    
    public boolean buytransport () {
        return this.buytransport(Transway);
    }
    
    public boolean buytransport (int N) {
        return this.buytransport(Transway,N);
    }
    
    @Override
    public String toString(){
        return "Company (Oil : " + this.Oil + " , Money : " + this.Money + ")";
    }
    
    public void printsellOil(){
        System.out.println("Don't forget to bring some oil to me."); 
    }
    
    public void printbuyOil(){
        System.out.println("Don't forget to pay the attached money");
    }
}
    
