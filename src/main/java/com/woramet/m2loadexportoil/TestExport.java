/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.m2loadexportoil;

/**
 *
 * @author User
 */
public class TestExport {
    public static void main(String[] args) {
        Export export = new Export (10000,5000);
        System.out.println(export);
        export.selltransport("Land", 5);
        System.out.println(export);
        export.selltransport("Water");
        System.out.println(export);
        export.selltransport(10);
        System.out.println(export);
        export.selltransport();
        System.out.println(export);
        export.selltransport("Air", 9);
        System.out.println(export);
        export.buytransport("Air", 9);
        System.out.println(export);
        export.buytransport();
        System.out.println(export);
        export.buytransport(10);
        System.out.println(export);
        export.selltransport("Water", 100);
        System.out.println(export);
        export.buytransport("Water");
        System.out.println(export);
        export.buytransport("Land", 5);
        System.out.println(export);
    }
    
}
